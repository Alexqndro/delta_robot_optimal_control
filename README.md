# CNOEC Project: Optimal trajectory of a Delta Robot
1. Uriel Guadarrama Ramirez
2. Rodrigo Lopez Gomez
3. Alessandro Del Duca

# Repository branches 
0. master
1. testing
2. testing_yalmip

## Abstract
The case under study in this report will be a 3-DOF Delta robot, a type of parallel robot that consists of three arms connected to universal joints at the base.
Delta robots have popular usage in pick and place and packaging applications, mainly used in factories thanks to their high velocity and precision, that can be up to 300 picks per minute.
To reach such positioning precision and velocities, typically a PID is used to control the motor torques, in this work we will present an alternative non linear MPC based on the solution of a Finite Horizon Optimal Control Problem, casted as optimization program and solved through a SQP based solver written by us. 
The project is part of the exam "Constrained Numerical Optimization for Estimation and Control" by professor Lorenzo Mario Fagiano at the Politecnico di Milano. 

## Report
The report can be found at the link: 

## Folders
- Delta_myfmincon: This folder contain the non-linear Program solver based on Sequential Quadratic Programming with interior point method
- Delta_trajectory_generation: This folder contain the Kinematics equations of the robot and some functions to generate and plot the trajectory
- Delta_dynamics: This folder contain the dynamic model of the system, the model parameters, and the function that compute the analytical jacobian
- Delta_optimization: This folder contain the cost functions, the error functions and other functions needed for constructing the linear constraints

## Delta_main
This is the main script, that call the optimization. One can chose to control the robot with different trajectories and to solve the problem with a BFGS method or a Gauss Newton method. To do so, change these variables according to the following values.
- trajectory = 'Circle'
- trajectory = 'Pick'
- method = 'BFGS' 
- method = 'GN'
