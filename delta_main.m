clearvars; close all; clc

path = genpath(pwd);
addpath(path);
addpath('Delta_myfmincon')
addpath('Delta_dynamics')
addpath('Delta_trajectory_generation')
addpath('Delta_optimization')

param = delta_param;

%% Trajectory generation
trajectory = 'Pick';
method     = 'GN';

if strcmp(trajectory, 'Pick')
    load('pick_traj');
    Np       =  length(ref_traj);
    xiref    =  ref_traj(1:6,:);
    % Inizialize initial and final state
    xio      =      ref_traj(1:6,1);
    xiend    =      ref_traj(1:6,Np);
elseif strcmp(trajectory, 'Circle')
    load('circle_traj')
    Np       =  length(ref_traj);
    xiref    =  ref_traj(1:3,:);
    % Inizialize initial and final state
    xio      =      ref_traj(1:3,1);
    xiend    =      ref_traj(1:3,Np);
end



%% Simulation parameters
dt       =      0.002;              % seconds, input sampling period
Tend     =      0.5;                % seconds, terminal time
tvec_FFD =      0:dt:Tend;          % time vector (s)

%% Initialization
% Initialize optimization variables
uin      =       rand(3*Np,1);

%% Constraints
% Bounds on input variables
[A,b,C,d] = delta_constrain(param.lbnd, param.ubnd, Np);
p = 6;
q = Np*3*2;

if strcmp(method,'BFGS')
    %% Solution -  BFGS
    % Initialize solver options
    myoptions               =   myoptimset;
    myoptions.Hessmethod  	=	'BFGS';
    myoptions.gradmethod  	=	'CD';
    myoptions.graddx        =	2^-17;
    myoptions.tolgrad    	=	1e-8;
    myoptions.ls_beta       =	0.5;
    myoptions.ls_c          =	0.1;
    myoptions.ls_nitermax   =	1e2;
    myoptions.nitermax      =	100;
    myoptions.xsequence     =	'on';

elseif strcmp(method,'GN')
    myoptions               =   myoptimset;
    myoptions.Hessmethod  	=	'GN';
    myoptions.gradmethod  	=	'CD';
    myoptions.graddx        =	2^-17;
    myoptions.tolgrad    	=	1e-8;
    myoptions.ls_beta       =	0.5;
    myoptions.ls_c          =	0.1;
    myoptions.ls_nitermax   =	1e2;
    myoptions.nitermax      =	30;
    myoptions.xsequence     =	'on';
    if strcmp(trajectory, 'Pick')
            myoptions.GN_funF   =   @(uin)delta_err_pick(uin, xiref, dt, param);
    elseif strcmp(trajectory, 'Circle')
            myoptions.GN_funF   =   @(uin)delta_err_circle(uin, xiref, dt, param);
    end
end

% Run solver
if strcmp(trajectory, 'Pick')
    tic
    [ustar,fxstar,niter,exitflag,xsequence] = myfmincon(@(uin)delta_cost_pick(uin, xiref, dt, param ),uin,A,b,C,d,p,q,myoptions);
    toc
    %% Results 
    delta_plot_results_pick(ustar, xiref, param, dt)
elseif strcmp(trajectory, 'Circle')
    % Run solver
    tic
    [ustar,fxstar,niter,exitflag,xsequence] = myfmincon(@(uin)delta_cost_circle(uin, xiref, dt, param ),uin,A,b,C,d,p,q,myoptions);
    toc
    %% Results 
    err = delta_plot_results_circle(ustar, xiref, param, dt);
end
