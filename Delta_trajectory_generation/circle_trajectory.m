function EE_traj = circle_trajectory(Co,r)
%%  Reference trajectory computation
%
% This function implement trajectory reference
% 
% INPUT:
%          - Co vector           [x,y,z] in meters, center of the revolute
%          - r                   in meters, radius of the revolute
%
%
% OUTPUT:
%          - EE_traj             vector [x_circle, y_circle, z_circle] in meters             
%                                 - Vector [3xm], where m is the number of
%                                 referente trajectory points
% Example revolute [x,y,z] = circle_trajectory(0,0,-1,0.5);
x = Co(1); y = Co(2); z = Co(3);

% Trajectory computation
th       = 0:pi/50:2*pi;
x_circle = r * cos(th) + x;
y_circle = r * sin(th) + y;
z_circle = 0.005*sin(2*th) + z;

EE_traj = [x_circle; y_circle; z_circle];

% Plot
figure
plot3(x_circle, y_circle,z_circle);
axis([-0.1 0.1 -0.1 0.1 -0.2 0.05]);xlabel('x');ylabel('y');zlabel('z');
title('End effector reference trajectory');
grid on;

end
