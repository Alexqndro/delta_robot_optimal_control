function  PlotPosition(r, t, p)
% This function plot the robot and the reference trajectory
%
% INPUT:
%          - r                   [3xm] pose of the robot in meters,
%                                m -number of trajectory points
%          - t                   [3xm] active angles 
%          - param               parameters of the robot  
%
% OUTPUT:
%          - None
%
%% init
%coords
x = r(1); y = r(2); z = r(3);
t1 = t(1); t2 = t(2); t3 = t(3);


rf = p.rf;  re = p.re;
f = p.f;    e = p.e;

%% Plot
%init
grid on
l = 0.75*(f + 2*rf); %to adjust the view
axis([-l l -l l -2*l l*0.5])
cla;

%calc and plot robot f plate
P_f1 =[f, f/tand(30), 0];
P_f2 =[f, -f/tand(30), 0];
P_f3 =[-f/sind(30),0,0];

%Upper triangle plate
line([P_f1(1) P_f2(1)],[P_f1(2) P_f2(2)],[P_f1(3) P_f2(3)],'LineWidth',1.4)
line([P_f2(1) P_f3(1)],[P_f2(2) P_f3(2)],[P_f2(3) P_f3(3)],'LineWidth',1.4)
line([P_f3(1) P_f1(1)],[P_f3(2) P_f1(2)],[P_f3(3) P_f1(3)],'LineWidth',1.4)

hold on
%calc and plot robot e plate
P_e1 = r' + [e, e/tand(30), 0];
P_e2 = r' + [e, -e/tand(30), 0];
P_e3 = r' + [-e/sind(30),0,0];

%lower triangle, moving plate
line([P_e1(1) P_e2(1)],[P_e1(2) P_e2(2)],[P_e1(3) P_e2(3)],'LineWidth',1.4)
line([P_e2(1) P_e3(1)],[P_e2(2) P_e3(2)],[P_e2(3) P_e3(3)],'LineWidth',1.4)
line([P_e3(1) P_e1(1)],[P_e3(2) P_e1(2)],[P_e3(3) P_e1(3)],'LineWidth',1.4)

hold on

plot3(x,y,z,'o','color','green')%r-pos

%calc and plot joints f
P_F1 = [f,0,0];
%use rotation matrix to calc other points
deg = 120;
R = [cos(deg*pi/180),-sin(deg*pi/180),0;...
     sin(deg*pi/180),cos(deg*pi/180),0;...
     0,0,1];
P_F2 = (R*P_F1')';
P_F3 = (R*P_F2')';

hold on
plot3(P_F1(1),P_F1(2),P_F1(3),'o','color','green')%r-pos
hold on
plot3(P_F2(1),P_F2(2),P_F2(3),'o','color','green')%r-pos
hold on
plot3(P_F3(1),P_F3(2),P_F3(3),'o','color','green')%r-pos
hold on

P_J1 =    [f+rf*cos(t1),0,-rf*sin(t1)];
P_J2 = (R*[f+rf*cos(t2),0,-rf*sin(t2)]')';
deg = -120;
R = [cos(deg*pi/180), -sin(deg*pi/180), 0;
     sin(deg*pi/180), cos(deg*pi/180),  0;
           0,               0,          1];
P_J3 = (R*[f+rf*cos(t3),0,-rf*sin(t3)]')';

line([P_F1(1) P_J1(1)],[P_F1(2) P_J1(2)],[P_F1(3) P_J1(3)],'color','red','LineWidth',2)
line([P_F2(1) P_J2(1)],[P_F2(2) P_J2(2)],[P_F2(3) P_J2(3)],'color','red','LineWidth',2)
line([P_F3(1) P_J3(1)],[P_F3(2) P_J3(2)],[P_F3(3) P_J3(3)],'color','red','LineWidth',2)

hold on
plot3(P_J1(1),P_J1(2),P_J1(3),'o','color','green')%r-pos
hold on
plot3(P_J2(1),P_J2(2),P_J2(3),'o','color','green')%r-pos
hold on
plot3(P_J3(1),P_J3(2),P_J3(3),'o','color','green')%r-pos
hold on

%calc and plot joint e
Transl = [e,0, 0];
P_E1   = Transl + [x,y,z];
deg    = 120;
R1     = [cos(deg*pi/180), -sin(deg*pi/180), 0;
          sin(deg*pi/180), cos(deg*pi/180),  0;
                0,               0,          1];
P_E2  =((R1*Transl') + r)';
deg   = -120;
R2    =[cos(deg*pi/180), -sin(deg*pi/180), 0;
        sin(deg*pi/180),  cos(deg*pi/180), 0;
              0,                0,         1];
P_E3  =((R2*Transl') + r)';

line([P_J1(1) P_E1(1)],[P_J1(2) P_E1(2)],[P_J1(3) P_E1(3)],'color','red','LineWidth',2)
line([P_J2(1) P_E2(1)],[P_J2(2) P_E2(2)],[P_J2(3) P_E2(3)],'color','c','LineWidth',2)
line([P_J3(1) P_E3(1)],[P_J3(2) P_E3(2)],[P_J3(3) P_E3(3)],'color','m','LineWidth',2)

hold on

% thetai_1 = [Theta11,Theta21,Theta31];
% thetai_2 = [Theta12,Theta22,Theta32];
% thetai_3 = [Theta13,Theta23,Theta33];
% 
% Animation for confirmation
P_J1_p = P_F1 + rf*[cos(t1),0, -sin(t1)];
P_J2_p = P_F2 + rf*[cos(t2),0, -sin(t2)]*R1';%*R1
P_J3_p = P_F3 + rf*[cos(t3),0,-sin(t3)]*R2';
line([P_F1(1) P_J1_p(1)],[P_F1(2) P_J1_p(2)],[P_F1(3) P_J1_p(3)],'color','blue','LineWidth',2)
line([P_F2(1) P_J2_p(1)],[P_F2(2) P_J2_p(2)],[P_F2(3) P_J2_p(3)],'color','blue','LineWidth',2)
line([P_F3(1) P_J3_p(1)],[P_F3(2) P_J3_p(2)],[P_F3(3) P_J3_p(3)],'color','blue','LineWidth',2)

% P_E1_p = [f+rf*cos(t1)+re*cos(t1+(80.8028*pi/180))*sind(81.5933),re*cosd(81.5933), -rf*sin(t1)-re*sin(t1+80.8028*pi/180)*sind(81.5933)];
% P_E2_p = [f+rf*cos(t2)+re*cos(t2+(81.7746*pi/180))*sind(94.1920),re*cosd(94.1920), -rf*sin(t2)-re*sin(t2+81.7746*pi/180)*sind(94.1920)]*R1';
% P_E3_p = [f+rf*cos(t3)+re*cos(t3+(79.9789*pi/180))*sind(94.1920),re*cosd(94.1920), -rf*sin(t3)-re*sin(t3+79.9789*pi/180)*sind(94.1920)]*R2';
% 
% line([P_J1_p(1) P_E1_p(1)],[P_J1_p(2) P_E1_p(2)],[P_J1_p(3) P_E1_p(3)],'color','blue','LineWidth',2)
% line([P_J2_p(1) P_E2_p(1)],[P_J2_p(2) P_E2_p(2)],[P_J2_p(3) P_E2_p(3)],'color','blue','LineWidth',2)
% line([P_J3_p(1) P_E3_p(1)],[P_J3_p(2) P_E3_p(2)],[P_J3_p(3) P_E3_p(3)],'color','blue','LineWidth',2)

view(45,30)
xlabel(['x = ' num2str(x)]);
ylabel(['y = ' num2str(y)]);
zlabel(['z = ' num2str(z)]);
end







