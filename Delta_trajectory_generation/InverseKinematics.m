function [ thetai_1,thetai_2,thetai_3 ] = InverseKinematics(r, param)
%% INVERSEKINEMATICS calculates all the angles given a single pose r
% This function computes the inverse kinematics for given a pose, it
% returns all the angles actives and pasives
% 
% INPUT:
%          - pose               [meter]             
%                                vector with the the pose of the robot (x,y,z)
%                                 - Vector [1 x 3]
%          - param               parameters of the robot 
% OUTPUT:
%          - thetai_1,thetai_2,thetai_3      [degrees]             
%                                vectors with the all the angles values
%                                 - Vectors of [1 x 3] each one
%% Init
deg    = -90;
R1     = [cos(deg*pi/180), -sin(deg*pi/180), 0;
          sin(deg*pi/180), cos(deg*pi/180),  0;
                0,               0,          1];
rp = (R1*r')';

x = rp(1);
y = rp(2);
z = rp(3);

%t11=0;
t21=0; t31=0;

[t11 ,s] = CalcInverse(rp,param);
if(s==0)
    [t21, s] = CalcInverse([x*cos(120*pi/180) + y*sin(120*pi/180), y*cos(120*pi/180)-x*sin(120*pi/180), z], param);
end

if(s==0)
    [t31, s] = CalcInverse([x*cos(120*pi/180) - y*sin(120*pi/180), y*cos(120*pi/180)+x*sin(120*pi/180), z], param);
end

if(s==0) 
    thetai_1 = [t11, t21, t31];
else
    thetai_1 = [0,0,0];
end
[thetai_2,thetai_3] = CalcPasiveAngles2( r, param);
end

function [thetai_1, s] = CalcInverse( r, p)
%CALCINVERSE
thetai_1 = 0;

%% Pose
x = r(1);
y = r(2);
z = r(3);

%%inverse kinematics
y1 = -p.f;
k = p.e;
y = y - k; %shift

a = (x^2 + y^2 + z^2 +p.rf^2 - p.re^2 - y1^2)/(2*z);
b = (y1 - y)/z;

d = -(a + b*y1)^2 + p.rf*(b^2*p.rf + p.rf); 
if(d<0)
   %disp('Pose not in range!!!!!!!!!!!!!!!!!!!! Choose other Pose that is not a singularity');
   s = 1;
else
    yj = (y1 - a*b - sqrt(d))/(b^2 + 1); % choosing outer point
    zj = a + b*yj;
    thetai_1 = 180*atan(-zj/(y1 - yj))/pi;

    if (yj > y1)
        thetai_1 = thetai_1 + 180;
    end
    s = 0;
end
end

function [theta2_i, theta3_i] = CalcPasiveAngles2(r, param)

px = r(1);
py = r(2);
pz = r(3);

phi1=0;
phi2=120*pi/180;
phi3=240*pi/180;

a=param.la;
b=param.lb;
r=param.f;
h=param.e;

%Pasive angle of fisrt arm - 1st
cx1=px*cos(phi1)+py*sin(phi1)+h-r;
cy1=-px*sin(phi1)+py*cos(phi1);
cz1=pz;

theta31=acos(cy1/b);

k=(cx1^2+cy1^2+cz1^2-a^2-b^2)/(2*a*b*sin(theta31));

theta21=acos(k);

%syms x;
%syms x1

%theta11=solve(a*cos(x)+b*sin(theta31)*cos(x+theta21)==cx1);
%theta111=solve(a*sin(x1)+b*sin(theta31)*sin(x1+theta21)==cz1);

%theta11=x;

%Pasive angle of second arm - 2nd
cx2=px*cos(phi2)+py*sin(phi2)+h-r;
cy2=-px*sin(phi2)+py*cos(phi2);
cz2=pz;

theta32=acos(cy2/b);

k2=(cx2^2+cy2^2+cz2^2-a^2-b^2)/(2*a*b*sin(theta32));

theta22=acos(k2);

%syms x2;

%theta12=solve(a*cos(x2)+b*sin(theta32)*cos(x2+theta22)==cx2);


%Pasive angle of thrid arm - 3rd
cx3=px*cos(phi3)+py*sin(phi3)+h-r;
cy3=-px*sin(phi3)+py*cos(phi3);
cz3=pz;

theta33=acos(cy3/b);

k3=(cx3^2+cy3^2+cz3^2-a^2-b^2)/(2*a*b*sin(theta33));

theta23=acos(k3);

%syms x3;

%theta13=solve(a*cos(x3)+b*sin(theta33)*cos(x3+theta23)==cx3);

theta2_i = [theta21,theta22,theta23];
theta3_i = [theta31,theta32,theta33];
theta2_i = theta2_i*180/pi;
theta3_i = theta3_i*180/pi;

end

