%% Trajectory generation
param = delta_param;

%% Simulation parameters
dt       =       0.005;                 % seconds, input sampling period
Tend     =       0.5;                   % seconds, terminal time
tvec_FFD =       0:dt:Tend;             % time vector (s)
Np       =       length(tvec_FFD);

%% Initialize optimization variables
uin      =       zeros(3*Np,1);

%% Inizialize initial and final state
xio      =  [60*pi/180;60*pi/180;60*pi/180;0;0;0];
%xio      =  [0.4917;0.7034;0.7034;0;0;0];
xiend    =  xio;

xi      = zeros(6,Np);
t       = zeros(1,Np);
xi(:,1) = xio;

%% Initialize simulation output
N_FFD     = length(tvec_FFD);           % number of samples
xiout_FFD = zeros(6,N_FFD);             % matrix with states
uout_FFD  = zeros(3,N_FFD);             % matrix with inputs
xiout_FFD(:,1) = xio;
%uout_FFD(:,1) = [-1;-2;-2];
u_1 = -3 * ones(1,N_FFD);
u_2 = -3 * ones(1,N_FFD);
u_3 = -3 * ones(1,N_FFD);
for i = 30:80
    u_1(1,i) = -1 ;
    u_2(1,i) = -2 ;
    u_3(1,i) = -2 ;
end
for i = 80:N_FFD
    u_1(1,i) = 2 ;
    u_2(1,i) = 2 ;
    u_3(1,i) = 2 ;
end
% load('ref_traj');
% u_1 = -4*ref_traj(1,:);
% u_2 = -4*ref_traj(2,:);
% u_3 = -4*ref_traj(3,:);
uout_FFD = [u_1;u_2;u_3];

r = zeros(3,N_FFD); % matrix with the pose x,y,z
r(:,1) =  ForwardKinematics(xio(1,:),xio(2,:),xio(3,:),param);

for i = 2:Np
    [t_temp, xi_temp] = ode45(@(t,xi)delta_dyn(t,xi,uout_FFD(:,i),param), [t(1,i-1),t(1,i-1)+dt], xi(:,i-1));
    t(1,i)  = t_temp(end);
    xi(:,i) = xi_temp(end,:)';
    r(:,i)  = ForwardKinematics(xi(1,i),xi(2,i),xi(3,i),param);
    
%     xidot = delta_dyn(0,xiout_FFD(:,i-1),uout_FFD(:,1),param);
%     xiout_FFD(:,i) = xiout_FFD(:,i-1) + dt*xidot;
%     r(:,i)  = ForwardKinematics(xiout_FFD(1,i),xiout_FFD(2,i),xiout_FFD(3,i),param);
end

close all
figure(1)
subplot 311;plot(tvec_FFD,xi(1,:)*180/pi);grid;xlabel('time [s]');xlabel('Time [s]');ylabel('Angle [�]');title('Reference Position joint q_1');
subplot 312;plot(tvec_FFD,xi(2,:)*180/pi);grid;xlabel('time [s]');xlabel('Time [s]');ylabel('Angle [�]');title('Reference Position joint q_2');
subplot 313;plot(tvec_FFD,xi(3,:)*180/pi);grid;xlabel('time [s]');xlabel('Time [s]');ylabel('Angle [�]');title('Reference Position joint q_3');
hold off
figure(2)
subplot 311;plot(tvec_FFD,xi(4,:)*180/pi);grid;xlabel('time [s]');xlabel('Time [s]');ylabel('Velocity [�/s]');title('Reference Velocity joint q_1');
subplot 312;plot(tvec_FFD,xi(5,:)*180/pi);grid;xlabel('time [s]');xlabel('Time [s]');ylabel('Velocity [�/s]');title('Reference Velocity joint q_2');
subplot 313;plot(tvec_FFD,xi(6,:)*180/pi);grid;xlabel('time [s]');xlabel('Time [s]');ylabel('Velocity [�/s]');title('Reference Velocity joint q_3');
hold off

% Animation(xiout_FFD(1:3,:),r,param)
figure(3)
Animation(xi(1:3,:),r,param)
plot3(r(1,:), r(2,:), r(3,:))
xlabel('x [m]');ylabel('y [m]');zlabel('z [m]');
hold off

%% Savefile
ref_traj = xi;
namefile = 'pick_traj';
save(namefile,'ref_traj')