close all;
%%  Reference trajectory computation
%
% This main implement the trajectory reference generation
%          - ref_trajectory      [degrees]             
%                                vector with the active joints values for the reference trajectory
%                                 - Vector [3 x M]
%% Trajectory generation
path = genpath(pwd);
addpath(path);
addpath('../CNO_myfmincon')
addpath('../Delta_dynamics')
addpath('../Delta_trajectory_gen')
p = delta_param;

% Revolute reference trajectory coordinates in meters
EE_traj = circle_trajectory([0, 0, -0.8], 0.1);
[angles_activ1,angles_pasiv2,angles_pasiv3] = CalcTrajectoryAngles(EE_traj, p); % active angles in degrees
angles_activ1 = angles_activ1*pi/180;
angles_pasiv2 = angles_pasiv2*pi/180;
angles_pasiv3 = angles_pasiv3*pi/180;

%% Plot
Animation(angles_activ1, EE_traj, p);
plot3(EE_traj(1,:), EE_traj(2,:), EE_traj(3,:));
xlabel('x [m]');ylabel('y [m]');zlabel('z [m]');
hold off

figure
dt=0.001;
N=length(angles_activ1);
tvet = 0:dt:(N-1)*dt;
ngles_activ1=angles_activ1*180/pi;
plot(tvet,ngles_activ1(1,:), tvet,ngles_activ1(2,:),tvet,ngles_activ1(3,:));xlabel('Time [s]');ylabel('Angle [�]');title('Reference Trajectory');legend('Active joint angle q_1','Active joint angle q_2','Active joint angle q_3');
%% Savefile
ref_traj = angles_activ1;
namefile = 'circle_traj';
save(namefile,'ref_traj')