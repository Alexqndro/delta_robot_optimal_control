function Animation( angles_act, traj, param )
%ANIMATION Make Plot animation
%!!!Precalcualte trajectory with forward kinematics

%simulation step size
N = size(angles_act,2);
dt = 0.0;

figure
for i=1:N 
    PlotPosition(traj(:,i),angles_act(:,i),param);
    pause(dt);
end

end

