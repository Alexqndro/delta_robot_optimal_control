function [ r ] = ForwardKinematics(t1, t2, t3, p)
%(t1 t2 t3) -> (x0 y0 z0)

%% init
%Virtual Joint Positions for first joint (which is directly connected to the motor):
k=(p.f - p.e);
J1=[0;-k - p.rf*cos(t1);-p.rf*sin(t1)]; %x1=0->projection on yz plane
J2=[( k + p.rf*cos(t2))*cos(30*pi/180);...
    ( k + p.rf*cos(t2))*sin(30*pi/180);...
    -p.rf*sin(t2)];
J3=[(-k - p.rf*cos(t3))*cos(30*pi/180);...
    ( k +p.rf*cos(t3))*sin(30*pi/180);...
    - p.rf*sin(t3)];

%proportional constants for end effector position
w1=J1(1)^2 + J1(2)^2 + J1(3)^2;
w2=J2(1)^2 + J2(2)^2 + J2(3)^2;
w3=J3(1)^2 + J3(2)^2 + J3(3)^2;

%calculate Ji positions
x1 = J1(1); x2 = J2(1); x3 = J3(1);
y1 = J1(2); y2 = J2(2); y3 = J3(2);
z1 = J1(3); z2 = J2(3); z3 = J3(3);

d  = (y2 - y1)*x3 - (y3 - y1)*x2;
a1 = (1/d)*((z2 - z1)*(y3 - y1) - (z3 - z1)*(y2 - y1));
b1 = (-1/(2*d))*((w2 - w1)*(y3 - y1)-(w3 - w1)*(y2 - y1));
a2 = (-1/d)*((z2 - z1)*x3 - (z3 - z1)*x2);
b2 = (1/(2*d))*((w2 - w1)*x3 - (w3 - w1)*x2);

%% numerics
%quadratic equation to solve->choose smaller negative value!
a = (a1^2 + a2^2 + 1);
b = 2*(a1*b1 + a2*(b2 - y1) - z1);
c = (b1^2 + (b2 - y1)^2 + z1^2 - p.re^2);
ztemp  = roots([a,b,c]);
if ztemp(1) < ztemp(2)
    z = ztemp(1);
else
    z = ztemp(2);
end

%insert z into:
x = @(z)a1*z+b1;
y = @(z)a2*z+b2;

%return position vector
r = eval('[x(z),y(z),z]');
deg    = -90;
R1     = [cos(deg*pi/180), -sin(deg*pi/180), 0;
          sin(deg*pi/180), cos(deg*pi/180),  0;
                0,               0,          1];
r = (r*R1);
%r = [x; y; z];

end

