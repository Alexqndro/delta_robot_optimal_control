function [ angles_activ1,angles_pasiv2,angles_pasiv3 ] = CalcTrajectoryAngles(traj, param)
%% Computation of angles of the robot
% This function computes the inverse kinematics for given a trajectory, it
% returns all the angles actives and pasives
% 
% INPUT:
%          - trajectory          [meters]             
%                                vector with the all the poses of the robot (x,y,z)
%                                 - Vector [3 x m], where m is the number
%                                 of points
%          - param               parameters of the robot  
% OUTPUT:
%          - angles_activ1,angles_pasiv2,angles_pasiv3           
%                                [degrees]             
%                                vectors with the all the angles values
%                                - Each vector is of dimension [3 x m], where m is the number
%                                 of points
%% Init
M = length(traj);
angles_activ1 = zeros(3,M);
angles_pasiv2 = zeros(3,M);
angles_pasiv3 = zeros(3,M);

for i=1:M
    [angles_activ1(:,i),angles_pasiv2(:,i),angles_pasiv3(:,i)] = InverseKinematics([traj(1,i),traj(2,i),traj(3,i)],param);
end

end

