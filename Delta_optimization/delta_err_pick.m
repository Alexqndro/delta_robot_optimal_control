function [F, xi, g, h] = delta_err_pick(uin, xiref, dt, param) 

%% Delta robot error computations
% This function compute the matrix F needed to compute the GN type cost
% function J = F'*F while providing the evolution of the system xi(t)
% 
% INPUT
% - uin                  Input torques on the active joints, our decision
%                        variables
%                        - Vector[3xN]
%
% - xi_ref               Reference trajectory that the delta robot should 
%                        follow
%                        - Vector[6xN]
%
% - xi0                  Delta robot starting conditions
%                        - Vector[6x1]
%
% - xiend                Delta robot final state
%                        - Vector[6x1]
%
% - p                    Struct containing all the parameters
%
% OUTPUT
% - F                    Vector of the squared errors
%                        - Vector[:x1]
%
% - xi                   Evolution of the dynamics
%                        - Vector[6xN]
% 
% - g                    Equality non linear constraints
%
% - h                    Inequality non linear constraints
%
%% Dynamic equations
N       = length(xiref);
xio     = xiref(:,1);
xiend   = xiref(:,N);
xi      = zeros(6,N);
t       = zeros(1,N);
xi(:,1) = xio;

% for i = 2:N
% [t_temp, xi_temp] = ode45(@(t,xi)delta_dyn(t,xi,uin(3*i-2:3*i,1),param), [t(1,i-1),t(1,i-1)+dt], xi(:,i-1));
% t(1,i)  = t_temp(end);
% xi(:,i) = xi_temp(end,:)';
% end
for i = 2:N
    xi(:,i) = xi(:,i-1) + dt*delta_dyn(0,xi(:,i-1),uin(3*i-2:3*i,1),param);
end

N   = length(t);
n   = 6;
m   = size(uin,1);

err = zeros(n*N,1);
pow = zeros(N,1);

%% Computation of the error 
% Trajectory error
for i = 1:N
    err(n*i-5: i*n,1) = sqrtm(param.Q_pick)*(xi(1:6,i) - xiref(:,i));
end

% Final state error
err_end = sqrtm(param.S)*(xi(1:6,N) - xiref(:,N));

% Power demand 
for i = 1:N
    pow(n*i-2:n*i,1) = sqrt(param.R)*sqrt(max(zeros(3,1), uin(3*i-2:3*i,1)).*abs(xi(4:6,i)));
end
% Computation of non linear equality constraint
g = xi(:,N) - xiend; 

% Computation of non linear inequality contstraint h(xi) >= 0
for i = 1:N
    h1(3*i-2:3*i,1) = xi(1:3,i) - param.qmin*pi/180*ones(3,1);
end
for i = 1:N
    h2(3*i-2:3*i,1) = - xi(1:3,i) + param.qmax*pi/180*ones(3,1);
end
h = [h1;h2];

% Computation of the function F

F = [err; err_end; pow];

end
