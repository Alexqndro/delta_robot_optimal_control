function [A,b,C,d] = delta_constrain(lb, ub, N)

%% Delta robot constraints
% This function build both the constraint inequalities and equalities
% 
% INPUT
% - lb      Joint torque lowerbound
%
% - ub      Joint torque upperbound
% 
% - N       Prediction windows
%
% OUTPUT
% - A       Linear equality constraint matrix
%
% - b       Linear equality constraint vector
%
% - C       Linear inequality constraint matrix
%
% - d       Linear inequality constraint vector, upper and lower bounds of
%           the control action

m  = 3;

%% Input saturation C*x >= d

d  = [lb*ones(N*m,1);-ub*ones(N*m,1)];
C  = zeros(length(d),m*N);
for i=1:N
    C(3*i-2:3*i,3*i-2:3*i) = eye(3);
end
C(m*N+1:end,:) = -C(1:m*N,1:m*N);

%% Input final constraint A*x = b
b  = zeros(3,1);
A  = zeros(length(b),m*(N));
A(:,m*N-2:m*N) = eye(3);
end

