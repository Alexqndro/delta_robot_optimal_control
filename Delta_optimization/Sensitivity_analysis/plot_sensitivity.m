function plot_sensitivity(data,time,param)

N  = length(data);
Np = length(data{1})-2;

for j = 1:N
    ustar = data{j}(1:Np);
    for i=1:Np/3
        Ux(j,i) = ustar(3*i-2);
        Uy(j,i) = ustar(3*i-1);
        Uz(j,i) = ustar(3*i);
    end
end

figure(6)
title('Torques')
subplot(3,1,1);plot(time,Ux,time,param.ubnd*ones(size(time)),time,param.lbnd*ones(size(time)));xlabel('Time [s]');ylabel('Torque tau_1 [Nm]');title('Torque on active joint q_1');xlim([0 time(end)]);
subplot(3,1,2);plot(time,Uy,time,param.ubnd*ones(size(time)),time,param.lbnd*ones(size(time)));xlabel('Time [s]');ylabel('Torque tau_2 [Nm]');title('Torque on active joint q_2');xlim([0 time(end)]);
subplot(3,1,3);plot(time,Uz,time,param.ubnd*ones(size(time)),time,param.lbnd*ones(size(time)));xlabel('Time [s]');ylabel('Torque tau_3 [Nm]');title('Torque on active joint q_3');xlim([0 time(end)]);
end
