clearvars; close all; clc
 
path = genpath(pwd);
addpath(path);
addpath('../../CNO_myfmincon')
addpath('../../Delta_dynamics')
addpath('../../Delta_trajectory_gen')

param = delta_param;

%% FHOCP parameters - single shooting

%% Trajectory generation
load('pick_traj');
Np       =  length(ref_traj);
xiref    =  ref_traj;


%% Simulation parameters
dt       =       0.002;             % seconds, input sampling period
Tend     =       0.5;               % seconds, terminal time
tvec_FFD =       0:dt:Tend;         % time vector (s)

pw = PoolWaitbar(100,'Waiting');
parfor j = 1:10
%% Initialize parameters
newxref = xiref;
newpara = param;
%% Initialize optimization variables
uin     =       1*rand(3*Np,1);

%% Inizialize initial and final state

xio      =  [newxref(:,1);0;0;0];
xiend    =  [newxref(:,Np);0;0;0];
%% Constraints
% Bounds on input variables
[A,b,C,d] = delta_constrain(newpara.lbnd, newpara.ubnd, Np);
p = 6;
q = Np*3*2;

%% Solution -  GN
% Initialize solver options
myoptions               =   myoptimset;
myoptions.Hessmethod  	=	'GN';
myoptions.GN_funF       =   @(uin)delta_err_pick(uin,newxref, dt, newpara);
myoptions.gradmethod  	=	'CD';
myoptions.graddx        =	2^-10;
myoptions.tolgrad    	=	1e-8;
myoptions.ls_beta       =	0.5;
myoptions.ls_c          =	0.1;
myoptions.ls_nitermax   =	1e2;
myoptions.nitermax      =	30;
myoptions.xsequence     =	'on';

% Run solver
tic
[ustar,fxstar,niter,exitflag,xsequence] = myfmincon(@(uin)delta_cost_pick(uin, newxref, dt, newpara),uin,A,b,C,d,p,q,myoptions);
T = toc

data{j} = [ustar;fxstar;T];
increment(pw);
end
delete(gcp('nocreate'))
save('pardata','data')
load('time.mat');
plot_sensitivity(data,t,param)