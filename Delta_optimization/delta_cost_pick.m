function [J, F, xi, g, h] = delta_cost_pick(uin, xiref, dt, param) 

%% Delta robot cost function
% Function that computes the output trajectory of the Delta robot
% and the quadratic cost given by the sum of squared weighted errors between
% the state and the reference trajectory, so that we can use GN methods

%%
[F, xi, g, h]      =   delta_err_pick(uin, xiref, dt, param);


%% Compute sum of squared errors
J                 =   [F'*F;g;h];
end
