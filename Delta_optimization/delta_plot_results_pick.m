function err = delta_plot_results_pick(ustar, xiref, param, dt)

Np        = length(xiref);
xi        = zeros(6,Np);
t         = zeros(1,Np);
r         = zeros(3,Np);
xi(:,1)   = xiref(:,1);
r(:,1)    = ForwardKinematics(xi(1,1),xi(2,1),xi(3,1),param);
rref(:,1) = ForwardKinematics(xiref(1,1),xiref(2,1),xiref(3,1),param);

for i = 2:Np
    [t_temp, xi_temp] = ode45(@(t,xi)delta_dyn(t,xi,ustar(3*i-2:3*i,1),param), [t(1,i-1),t(1,i-1)+dt], xi(:,i-1));
    t(1,i)    = t_temp(end);
    xi(:,i)   = xi_temp(end,:)';
    r(:,i)    = ForwardKinematics(xi(1,i),xi(2,i),xi(3,i),param);
    rref(:,i) = ForwardKinematics(xiref(1,i),xiref(2,i),xiref(3,i),param);
end

Animation(xi(1:3,:),r,param)
plot3(r(1,:), r(2,:), r(3,:))
plot3(rref(1,:), rref(2,:), rref(3,:))
xlabel('x [m]');ylabel('y [m]');zlabel('z [m]');
legend('Reference trajectory','Optimized trajectory');
title('Pick & Place reference trajectory');
hold off

Xi    = rad2deg(xi);
Xiref = rad2deg(xiref);
figure(2)
title('Active angles trajectory')
subplot(3,1,1);plot(t,Xi(1,:),t,Xiref(1,:),t,param.qmax*ones(size(t)),t,param.qmin*ones(size(t)));xlabel('Time [s]');ylabel('Angle q_1 [°]');title('Active joint angle q_1');legend('Active joint angle q_1','Reference trajectory q_1_{ref}','Upper bound', 'Lower bound');xlim([0 t(end)]);
subplot(3,1,2);plot(t,Xi(2,:),t,Xiref(2,:),t,param.qmax*ones(size(t)),t,param.qmin*ones(size(t)));xlabel('Time [s]');ylabel('Angle q_2 [°]');title('Active joint angle q_2');legend('Active joint angle q_2','Reference trajectory q_2_{ref}','Upper bound', 'Lower bound');xlim([0 t(end)]);
subplot(3,1,3);plot(t,Xi(3,:),t,Xiref(3,:),t,param.qmax*ones(size(t)),t,param.qmin*ones(size(t)));xlabel('Time [s]');ylabel('Angle q_3 [°]');title('Active joint angle q_3');legend('Active joint angle q_3','Reference trajectory q_3_{ref}','Upper bound', 'Lower bound');xlim([0 t(end)]);

figure(3)
title('Angle errors')
subplot(3,1,1);plot(t,Xi(1,:)-Xiref(1,:));xlabel('Time [s]');ylabel('Error q_1 - q_1_{ref} [°]');title('Error between active joint angle q_1 and q_1_{ref}');xlim([0 t(end)]);
subplot(3,1,2);plot(t,Xi(2,:)-Xiref(2,:));xlabel('Time [s]');ylabel('Error q_2 - q_2_{ref} [°]');title('Error between active joint angle q_2 and q_2_{ref}');xlim([0 t(end)]);
subplot(3,1,3);plot(t,Xi(3,:)-Xiref(3,:));xlabel('Time [s]');ylabel('Error q_3 - q_3_{ref} [°]');title('Error between active joint angle q_3 and q_3_{ref}');xlim([0 t(end)]);

figure(4)
title('Angular velocities trajectory')
subplot(3,1,1);plot(t,Xi(4,:),t,Xiref(4,:));xlabel('Time [s]');ylabel('Velocity dq_1/dt [°/s]');title('Active joint angular velocity dq_1/dt');xlim([0 t(end)]);legend('Active joint velocity dq_1/dt','Reference velocity dq_1/dt_{ref}');
subplot(3,1,2);plot(t,Xi(5,:),t,Xiref(5,:));xlabel('Time [s]');ylabel('Velocity dq_2/dt [°/s]');title('Active joint angular velocity dq_2/dt');xlim([0 t(end)]);legend('Active joint velocity dq_2/dt','Reference velocity dq_2/dt_{ref}');
subplot(3,1,3);plot(t,Xi(6,:),t,Xiref(6,:));xlabel('Time [s]');ylabel('Velocity dq_3/dt [°/s]');title('Active joint angular velocity dq_3/dt');xlim([0 t(end)]);legend('Active joint velocity dq_3/dt','Reference velocity dq_3/dt_{ref}');

figure(5)
title('Velocity errors')
subplot(3,1,1);plot(t,Xi(4,:)-Xiref(4,:));xlabel('Time [s]');ylabel('Error dq_1/dt - dq_1/dt_{ref} [°/s]');title('Error between active joint velocity q_1 and q_1_{ref}');xlim([0 t(end)]);
subplot(3,1,2);plot(t,Xi(5,:)-Xiref(5,:));xlabel('Time [s]');ylabel('Error dq_2/dt - dq_2/dt_{ref} [°/s]');title('Error between active joint velocity q_2 and q_2_{ref}');xlim([0 t(end)]);
subplot(3,1,3);plot(t,Xi(6,:)-Xiref(6,:));xlabel('Time [s]');ylabel('Error dq_3/dt - dq_3/dt_{ref} [°/s]');title('Error between active joint velocity q_3 and q_3_{ref}');xlim([0 t(end)]);

U   = zeros(3,Np);
for i=1:Np
    U(:,i) = ustar(3*i-2:3*i);
end
figure(6)
title('Torques')
subplot(3,1,1);plot(t,U(1,:),t,param.ubnd*ones(size(t)),t,param.lbnd*ones(size(t)));xlabel('Time [s]');ylabel('Torque tau_1 [Nm]');title('Torque on active joint q_1');legend('Torque on active joint q_1','Upper bound', 'Lower bound');xlim([0 t(end)]);
subplot(3,1,2);plot(t,U(2,:),t,param.ubnd*ones(size(t)),t,param.lbnd*ones(size(t)));xlabel('Time [s]');ylabel('Torque tau_2 [Nm]');title('Torque on active joint q_2');legend('Torque on active joint q_2','Upper bound', 'Lower bound');xlim([0 t(end)]);
subplot(3,1,3);plot(t,U(3,:),t,param.ubnd*ones(size(t)),t,param.lbnd*ones(size(t)));xlabel('Time [s]');ylabel('Torque tau_3 [Nm]');title('Torque on active joint q_3');legend('Torque on active joint q_3','Upper bound', 'Lower bound');xlim([0 t(end)]);

err = mean((Xi-Xiref),2);
end

