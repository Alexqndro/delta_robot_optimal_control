function J = delta_jacobian(theta1, theta2, theta3)

%%  Delta robot Jacobian computation
%
% This function implement the jacobian computation of the dynamic of a 
% delta robot:
% 
% INPUT:
% - thetai               Angular displacement of the active and 
%                        passive joints of the i-th arm
%                        - Vector [3x1]
%
% OUTPUT:
% - J                    Jacobian matrix
%                        - Matrix [3x3]

%% Jacobian computation
p = delta_param;

phi1 = 0; %%%%%%%%%% double check
phi2 = 120*pi/180;
phi3 = 240*pi/180;

% phi1 = -90; %%%%%%%%%% new proposal
% phi2 = 30;
% phi3 = 150;
% 
% phi1 = 30; %%%%%%%%%% new proposal
% phi2 = 150;
% phi3 = 270;

% theta11 = theta1(1);
% theta21 = theta1(2);
% theta31 = theta1(3);
% theta12 = theta2(1);
% theta22 = theta2(2);
% theta32 = theta2(3);
% theta13 = theta3(1);
% theta23 = theta3(2);
% theta33 = theta3(3);

theta11 = theta1(1);
theta12 = theta1(2);
theta13 = theta1(3);
theta21 = theta2(1);
theta22 = theta2(2);
theta23 = theta2(3);
theta31 = theta3(1);
theta32 = theta3(2);
theta33 = theta3(3);

jq  =    p.la*[sin(theta21)*sin(theta31)   0                          0;
               0                           sin(theta22)*sin(theta32)  0;
               0                           0                          sin(theta23)*sin(theta33)];


j1x = cos(theta11+theta21)*sin(theta31)*cos(phi1)-cos(theta31)*sin(phi1);
j2x = cos(theta12+theta22)*sin(theta32)*cos(phi2)-cos(theta32)*sin(phi2);
j3x = cos(theta13+theta23)*sin(theta33)*cos(phi3)-cos(theta33)*sin(phi3);

j1y = cos(theta11+theta21)*sin(theta31)*sin(phi1)+cos(theta31)*cos(phi1);
j2y = cos(theta12+theta22)*sin(theta32)*sin(phi2)+cos(theta32)*cos(phi2);
j3y = cos(theta13+theta23)*sin(theta33)*sin(phi3)+cos(theta33)*cos(phi3);

j1z = sin(theta11+theta21)*sin(theta31);
j2z = sin(theta12+theta22)*sin(theta32);
j3z = sin(theta13+theta23)*sin(theta33);

jx = [j1x j1y j1z; 
      j2x j2y j2z;
      j3x j3y j3z];
  
 J   = jx \ jq; 
end