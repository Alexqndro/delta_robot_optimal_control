function xi_dot = delta_dyn(t, xi, tau, p)  %#ok<INUSL>

%%  Delta robot dynamic equations
%
% This function implement the equations of the dynamic of a delta robot:
% xi_dot = delta_dyn(xi,tau)
% 
% INPUT:
% - t                    Time, needed to use ode methods   
%
% - xi = [ theta11;      State of the system, composed by angular 
%          theta12;      displacement and velocities of the active
%          theta13;      joints. 
%          th11_dot;
%          th12_dot;
%          th13_dot;]   - Vector [6x1]
%
% - tau                  Torques applied to the active joints
%                        - Vector [3x1]
%
% - p                    Struct containing all the parameters
%
% OUTPUT:
% - xi_dot = [th11_dot;   Time derivative of the system's states
%             th12_dot;  
%             th13_dot;
%             th11_2dot;
%             th12_2dot;
%             th13_2dot;] - Vector [6x1]      

%%
I = eye(3);

theta11 = xi(1);                 % The first three states of the system are
theta12 = xi(2);                 % the three active joints displacements
theta13 = xi(3);

r = ForwardKinematics(theta11, theta12, theta13, p);

[theta1, theta2, theta3] = InverseKinematics(r, p);
theta1 = theta1*pi/180;%theta actives
theta2 = theta2*pi/180;%theta pasive 2
theta3 = theta3*pi/180;%theta pasive 3

Mga = (1/2 * p.ma + p.mb) * p.g * p.la * I * [cos(theta11); cos(theta12); cos(theta13)];   

Fgp = [0; 0; -(p.mtcp + 3*p.mb)*p.g]; 

Ia  = (1/3*p.ma*p.la^2 + p.mb*p.la^2)*I;

Mp  = (p.mtcp + 3*p.mb) * I;
    
J  = delta_jacobian(theta1, theta2, theta3);
    
%% Computation of the dynamical matrices
Mtheta = Ia + (J.') * Mp * J;       % Inertial matrix

Vtheta = (J.')*Mp * J;              % Centrifugal and Corioli forces matrix

Gtheta = -Mga-((J.')*Fgp);          % Gravity vector

%% Dynamic equations
xi_dot = [  xi(4:6);
            Mtheta \ (tau - Vtheta*xi(4:6) - Gtheta);];
end
