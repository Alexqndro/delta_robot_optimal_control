function param = delta_param

%% Delta robot parameters:
%
% This function create a struct where all the usefull parameters of the 
% robots are stored
% 
% OUTPUT:
% - param  Struct containing all the parameters
%
% NOTE:    Call the function: name_struct = delta_param
%          Then access the parameter of interest with name_struct.parameter

%% Mass and inertia parameters
param.ma    = 1.59;          % mass of upper link [kg]
param.mb    = 0.04;          % mass of lower link [kg]
param.mtcp  = 0.11;          % mass mobile platform [kg]
param.g     = 9.81;          % gravity acceleration [kg] 

%% Geometric parameters
param.la    = 334e-3;          % longitud upper link [m]
param.lb    = 684e-3;          % longitud lower link [m]
param.rf    = 334e-3;          % longitud upper link [m]
param.re    = 684e-3;          % longitud lower link [m]

%triangular side length in m:
param.f     = (185e-3/2)*tand(30);         % Base to Joint rod length [m]
param.e     = (44e-3/2)*tand(30);          % Joint to end effector rod length [m]

%% Scaling factors
param.scale = [1;1;1;1;1;1];
%% Motor saturation
param.lbnd    = -50;         % joint torque lower bound N-m
param.ubnd    = 50;          % joint torque upper bound N-m

%% Joint maximum and minimum angle
param.qmax    = 120;         % joint maximum angle [°]
param.qmin    = -30;         % joint minimum angle [°]
%% Optimization parameters - Tracking problem
wght_p         = 10;                                                   % trajectory position error wheight
wght_v         = 1e-3;                                                 % trajectory speed error wheight
param.Q_pick   = diag([wght_p wght_p wght_p wght_v wght_v wght_v]);    % trajectory error wheight
param.Q_circle = diag([wght_p wght_p wght_p]);                         % trajectory error wheight
param.S        = 1e-1;                                                 % final position error wheight
param.R        = 0;                                                    % power demand wheight

%% Optimization parameters - Minimum energy problem
% wght_p          = 100;                                                      % trajectory position error wheight
% wght_v          = 1e-3;                                                    % trajectory speed error wheight
% param.Q_pick    = diag([wght_p wght_p wght_p wght_v wght_v wght_v]);       % trajectory error wheight
% param.Q_circle  = diag([wght_p wght_p wght_p]);                            % trajectory error wheight
% param.S         = 1e-1;                                                    % final position error wheight
% param.R         = 1e-3;                                                    % power demand wheight

end
